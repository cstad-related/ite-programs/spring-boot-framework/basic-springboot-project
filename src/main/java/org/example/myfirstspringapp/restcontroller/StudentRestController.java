package org.example.myfirstspringapp.restcontroller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.example.myfirstspringapp.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
//@RequiredArgsConstructor -> constructor injection
//@Data -> field injection
public class StudentRestController {
//    this is called Field Injection
//    @Autowired
//    @Qualifier("studentServiceImpl2")
//    StudentService studentService ;
    // Constructor Injection
//   @Qualifier("studentServiceImpl1")
//   final StudentService studentService;
    StudentService studentService;
    @Autowired
    public void setStudentService(
            @Qualifier("studentServiceImpl1") StudentService studentService
    ) {
        this.studentService = studentService;
    }

    @GetMapping("/create-student")
    public String createStudent() {
        studentService.createStudent();
        return "Student is created !! ";
    }


}
