package org.example.myfirstspringapp.configuration;


import org.example.myfirstspringapp.service.TeacherService;
import org.example.myfirstspringapp.service.serviceImpl.TeacherServiceImpl;
import org.example.myfirstspringapp.service.serviceImpl.TeacherServiceImpl2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class ApplicationConfig {

    @Bean("teacherServiceImpl22")
    public TeacherService teacherServiceImpl2(){
        return  new TeacherServiceImpl2();
    }
}
