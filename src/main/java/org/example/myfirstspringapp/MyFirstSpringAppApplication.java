package org.example.myfirstspringapp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

//implements CommandLineRunner
@Slf4j
@SpringBootApplication
public class MyFirstSpringAppApplication implements CommandLineRunner {
    // just declare what we want to use , spring will inject the dependencies related to this for us , that's why
//    @Autowired
//    BeanLister beanLister ;


    @Value("${admin.username}")
    String username;

    @Value("${admin.password}")
    String password;

//    @Value("classpath:helloworld.txt")
//    Resource fileResource;
    @Value("${filelocation}")
    Resource fileResource;


// inject resource loader
    @Autowired
    ResourceLoader resourceLoader;
    public static void main(String[] args) {
        SpringApplication.run(MyFirstSpringAppApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception{
        Resource newResource = resourceLoader.getResource("classpath:helloworld.txt");
//        beanLister.listAllBean();
        System.out.println("Admin username is : " + username);
        System.out.println("Admin password is : " + password);
        try (
//                BufferedReader reader = new BufferedReader(
//                        new InputStreamReader(fileResource.getInputStream()))
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(newResource.getInputStream()))
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException ex) {
            log.info("An error occurred: {}", ex.getMessage());
        }

    }
}
