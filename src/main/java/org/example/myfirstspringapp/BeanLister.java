package org.example.myfirstspringapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Component
public class BeanLister {
//    DI , Dependencies Injection through the field
    @Autowired
    ApplicationContext context;
    //PersonRepository
    public  void listAllBean(){
        String[] allBeans = context.getBeanDefinitionNames();
        System.out.println("All Beans in this application");
        int count =0;
        for(var bean: allBeans){
            System.out.println(++count+". "+bean);
        }

    }
}
