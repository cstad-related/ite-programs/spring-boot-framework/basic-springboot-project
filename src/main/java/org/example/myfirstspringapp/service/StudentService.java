package org.example.myfirstspringapp.service;

import org.springframework.stereotype.Service;

//@Service
public interface StudentService {
    void createStudent();
    void deleteStudent();

}
