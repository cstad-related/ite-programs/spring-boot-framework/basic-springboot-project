package org.example.myfirstspringapp.service.serviceImpl;

import org.example.myfirstspringapp.service.StudentService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


@Service
//@Primary
public class StudentServiceImpl1 implements StudentService {

    // repo
    @Override
    public void createStudent() {
        System.out.println("This is the first version of creating student! ");
    }

    @Override
    public void deleteStudent() {
        System.out.println("This is the first version of deleting student!");
    }
}
