package org.example.myfirstspringapp.service.serviceImpl;

import org.example.myfirstspringapp.service.TeacherService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class TeacherServiceImpl implements TeacherService {
    //    can be related to repository
    @Override
    public void createSubject() {
        System.out.println("Subject is createdddd !");
    }

    @Override
    public void deleteSubject() {
        System.out.println("Subject is deleteddd! ");
    }
}
