package org.example.myfirstspringapp.service.serviceImpl;

import org.example.myfirstspringapp.service.TeacherService;
import org.springframework.stereotype.Service;


@Service
public class TeacherServiceImpl2 implements TeacherService {
    @Override
    public void createSubject() {
        System.out.println("Create subject version 2 ");
    }

    @Override
    public void deleteSubject() {
        System.out.println("Delete Subject Version 2 ");

    }
}
