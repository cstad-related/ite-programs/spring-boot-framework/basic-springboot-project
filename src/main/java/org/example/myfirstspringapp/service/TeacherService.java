package org.example.myfirstspringapp.service;

public interface TeacherService {
    void createSubject();
    void deleteSubject();
}
